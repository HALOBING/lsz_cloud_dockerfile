FROM jdk:8

RUN apt-get update \
 && apt-get install -f -y git curl gnupg wget \
 && rm -rf /var/lib/apt/lists/*

#RUN yum install -y git curl

ARG user=root
#ARG user=jenkins
#ARG group=jenkins
#ARG uid=1000
#ARG gid=1000
ARG http_port=8080
ARG agent_port=50000
ARG JENKINS_HOME=/var/jenkins_home
ARG M2_REPOSITORY=/var/jenkins_home/m2_repository
ARG GRADLE_USER_HOME=/var/jenkins_home/gradle_repository

ENV JENKINS_HOME $JENKINS_HOME
ENV JENKINS_SLAVE_AGENT_PORT ${agent_port}

# maven & gradle local repository
ENV M2_REPOSITORY $M2_REPOSITORY
ENV GRADLE_USER_HOME $GRADLE_USER_HOME

#设置时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone

# Jenkins is run with user `jenkins`, uid = 1000
# If you bind mount a volume from the host or a data container,
# ensure you use the same uid
#RUN mkdir -p $JENKINS_HOME \
  #&& chown ${uid}:${gid} $JENKINS_HOME \
  #&& groupadd -g ${gid} ${group} \
  #&& useradd -d "$JENKINS_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user}

#用root 否则在linux环境中比较麻烦 -- [FATAL tini (8)] exec /usr/local/bin/jenkins.sh failed: Permission denied
RUN mkdir -p $JENKINS_HOME \
	&& chmod -R 777 $JENKINS_HOME

#maven repository path
RUN mkdir -p M2_REPOSITORY \
	&& chmod -R 777 M2_REPOSITORY

#gradle repository path
RUN mkdir -p GRADLE_USER_HOME \
	&& chmod -R 777 GRADLE_USER_HOME

# Jenkins home directory is a volume, so configuration and build history
# can be persisted and survive image upgrades
VOLUME $JENKINS_HOME

# `/usr/share/jenkins/ref/` contains all reference configuration we want
# to set on a fresh new installation. Use it to bundle additional plugins
# or config file with your custom jenkins Docker image.
RUN mkdir -p /usr/share/jenkins/ref/init.groovy.d

# Use tini as subreaper in Docker container to adopt zombie processes
ARG TINI_VERSION=v0.16.1
COPY tini_pub.gpg ${JENKINS_HOME}/tini_pub.gpg
#RUN curl -fsSL https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-$(dpkg --print-architecture) -o /sbin/tini \
#  && curl -fsSL https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-$(dpkg --print-architecture).asc -o /sbin/tini.asc \
COPY tini/tini-static-amd64 /sbin/tini
COPY tini/tini-static-amd64.asc /sbin/tini.asc
RUN gpg --import ${JENKINS_HOME}/tini_pub.gpg \
  && gpg --verify /sbin/tini.asc \
  && rm -rf /sbin/tini.asc /root/.gnupg \
  && chmod +x /sbin/tini

#ENV TINI_VERSION v0.18.0
#ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /sbin/tini
#ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini.asc /sbin/tini.asc
#RUN gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 \
#   && gpg --batch --verify /sbin/tini.asc /sbin/tini \
#   && rm -rf /sbin/tini.asc /root/.gnupg \
#   && chmod +x /sbin/tini

COPY init.groovy /usr/share/jenkins/ref/init.groovy.d/tcp-slave-agent-port.groovy

# jenkins version being bundled in this docker image
ARG JENKINS_VERSION
ENV JENKINS_VERSION ${JENKINS_VERSION:-2.164.3}

# jenkins.war checksum, download will be validated using it
ARG JENKINS_SHA=5bb075b81a3929ceada4e960049e37df5f15a1e3cfc9dc24d749858e70b48919

# Can be used to customize where jenkins.war get downloaded from

#ARG JENKINS_URL=http://mirrors.shu.edu.cn/jenkins/war-stable/2.138.2/jenkins.war
ARG JENKINS_URL=https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/${JENKINS_VERSION}/jenkins-war-${JENKINS_VERSION}.war

# could use ADD but this one does not check Last-Modified header neither does it allow to control checksum
# see https://github.com/docker/docker/issues/8331


#RUN curl -fsSL ${JENKINS_URL} -o /usr/share/jenkins/jenkins.war \
  #&& echo "${JENKINS_SHA}  /usr/share/jenkins/jenkins.war" | sha256sum -c -

RUN curl -fsSL ${JENKINS_URL} -o /usr/share/jenkins/jenkins.war

ENV JENKINS_UC https://updates.jenkins.io
ENV JENKINS_UC_EXPERIMENTAL=https://updates.jenkins.io/experimental
ENV JENKINS_INCREMENTALS_REPO_MIRROR=https://repo.jenkins-ci.org/incrementals
RUN chown -R ${user} "$JENKINS_HOME" /usr/share/jenkins/ref

# for main web interface:
EXPOSE ${http_port}

# will be used by attached slave agents:
EXPOSE ${agent_port}

ENV COPY_REFERENCE_FILE_LOG $JENKINS_HOME/copy_reference_file.log
#
#
## 使用淘宝镜像安装Node.js v8.9.2
#RUN wget https://npm.taobao.org/mirrors/node/v8.9.2/node-v8.9.2-linux-x64.tar.gz && \
#    tar -C /usr/local --strip-components 1 -xzf node-v8.9.2-linux-x64.tar.gz && \
#    rm node-v8.9.2-linux-x64.tar.gz
#
## 安装cnpm
#RUN npm i -g cnpm --registry=https://registry.npm.taobao.org
#
## install yarn
#RUN cnpm install -g yarn react-native-cli && \
#    yarn config set registry https://registry.npm.taobao.org --global && \
#    yarn config set disturl https://npm.taobao.org/dist --global
#
## install cordova
#RUN cnpm install -g cordova@8.1.2
#
## hot update cli
###############################################################
## 注意：
## cordova-hcp 暂时未用到ngrok模块，如果需要使用ngrok，直接替换为 ` RUN cnpm install -g cordova-hot-code-push-cli `
## 目前安装的cordova-hot-code-push-cli做了如下更改：
##   1、linux下 安装cordova-hot-code-push-cli，会远程下载ngrok二进制包，downlaod ngrok-stable-linux-amd64.zip 巨慢，
##      故将ngrok fork到https://gitee.com/HALOBING/ngrok.git并注释掉postinstall.js远程下载
##   2、将https://gitee.com/HALOBING/cordova-hot-code-push-cli.git package.json中ngrok指向https://gitee.com/HALOBING/ngrok.git
###############################################################
#RUN cnpm install -g  https://gitee.com/HALOBING/cordova-hot-code-push-cli.git
#
## install android sdk
#RUN mkdir -p opt/android/sdk \
#    && chown -R ${user} /opt/android/sdk \
#    && cd opt/android/sdk/ \
#    && curl https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip -o android-sdk.zip && unzip android-sdk.zip -d . && rm android-sdk.zip \
#    && yes | ./tools/bin/sdkmanager --licenses \
#    && ./tools/bin/sdkmanager "tools" \
#    && ./tools/bin/sdkmanager "build-tools;28.0.3" "build-tools;27.0.3" "build-tools;26.0.2" "build-tools;26.0.1" "build-tools;25.0.3" "build-tools;25.0.2" "build-tools;25.0.1" "build-tools;25.0.0" "build-tools;23.0.1" "platforms;android-28" "platforms;android-27" "platforms;android-26" "platforms;android-25" "platforms;android-23" "extras;android;m2repository" "extras;google;m2repository" --no_https \
#    && ./tools/bin/sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" --no_https \
#    && ./tools/bin/sdkmanager --list


# install maven
ADD apache-maven-3.5.4-bin.zip /tmp/
RUN unzip /tmp/apache-maven-3.5.4-bin.zip -d /usr/local && \
     chown -R ${user} /usr/local/apache-maven-3.5.4 && \
     rm -rf /tmp/apache-maven-3.5.4-bin.zip
# copy maven settings.xml
ADD settings.xml /usr/local/apache-maven-3.5.4/conf/

# install gradle
#RUN wget https://downloads.gradle.org/distributions/gradle-5.5.1-bin.zip && \
#     unzip gradle-5.5.1-bin.zip -d /usr/local && \
#     chown -R ${user} /usr/local/gradle-5.5.1 && \
#     rm -rf gradle-5.5.1-bin.zip

# install python3
RUN apt install python3 && apt install python3-pip
RUN mkdir -p ~/.pip && chown -R ${USER} ~/.pip/
RUN cat > ~/.pip/pip.conf <<EOF
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
EOF

RUN pip3 install --upgrade pip

# Setup environment
ENV ANDROID_HOME /opt/android/sdk
ENV MAVEN_HOME /usr/local/apache-maven-3.5.4
ENV GRADLE_HOME /usr/local/gradle-5.5.1
ENV PATH ${PATH}:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${MAVEN_HOME}/bin:${GRADLE_HOME}/bin:

RUN echo ANDROID_HOME="$ANDROID_HOME" >> /etc/environment
RUN echo GRADLE_HOME="$GRADLE_HOME" >> /etc/environment
RUN echo MAVEN_HOME="$MAVEN_HOME" >> /etc/environment
RUN echo GRADLE_USER_HOME="$GRADLE_USER_HOME" >> /etc/environment



USER ${user}

COPY jenkins-support /usr/local/bin/jenkins-support
COPY jenkins.sh /usr/local/bin/jenkins.sh
COPY tini-shim.sh /bin/tini

RUN chmod +x /usr/local/bin/jenkins-support \
 && chmod +x /usr/local/bin/jenkins.sh \
 && chmod +x /bin/tini \
 && chown -R ${user} /usr/local/bin

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/jenkins.sh"]

# from a derived Dockerfile, can use `RUN plugins.sh active.txt` to setup /usr/share/jenkins/ref/plugins from a support bundle
COPY plugins.sh /usr/local/bin/plugins.sh
COPY install-plugins.sh /usr/local/bin/install-plugins.sh

RUN chmod +x /usr/local/bin/plugins.sh \
 && chmod +x /usr/local/bin/install-plugins.sh \
 && chown -R ${user} /usr/local/bin

#restore backup
COPY backup_${JENKINS_VERSION}.tar.gz /tmp/
RUN cd /tmp \
    && unzip backup_${JENKINS_VERSION}.tar.gz \
    && chown -R ${user} backup_${JENKINS_VERSION} \
    #&& mv -f backup_${JENKINS_VERSION} $JENKINS_HOME
