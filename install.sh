#!/usr/bin/env bash

sys_setup() {
  # source /etc/profile 命令无效
  # bash #https://blog.csdn.net/buynow123/article/details/51774018
  # 窗口中选择否
  sudo dpkg-reconfigure dash

  # sources mirror
  sudo mv /etc/apt/sources.list /etc/apt/sources.list_bak2
  sudo cp sys/mirrors/sources.tsinghua.list /etc/apt/sources.list.d/sources.tsinghua.list

  sudo apt-get -y update
  sudo apt-get -y upgrade

  sudo apt-get install -y git unzip rar unrar vim curl exfat-utils python-pip python3-pip

  # pip
  mkdir -p ~/.pip && sudo chown -R ${USER} ~/.pip/

  sudo cat > ~/.pip/pip.conf <<EOF
[global] 
index-url = http://mirrors.aliyun.com/pypi/simple/ 
[install] 
trusted-host=mirrors.aliyun.com 
EOF

  pip3 install --upgrade pip
}

install_sys() {
  sys_setup
  sudo sh ./sys/docker.sh install
}

build_images(){
  #build jdk, docker-compose always skip it
  docker-compose build jdk
  #docker-compose up --build
  docker-compose up --d

  jenkins_version=$(docker ps | grep jenkins | grep -v 'grep' | awk '{print $2}')
  if [ -n "$jenkins_version" ]; then
    printf "waiting for jenkins."
    while [ -z $(docker exec jenkins bash -c "find /var/jenkins_home/ -name initialAdminPassword") ]
    do
      sleep 1
      printf "."
    done

    echo ""
    echo "jenkins init finished."
    path_list=$(find jenkins -name *.tar.gz)
    if [ ! -z $path_list ]; then
        echo "restore $path_list"
        path=${path_list%.tar.gz}
        docker exec jenkins bash -c "cp -rf /tmp/${path##*/}/* /var/jenkins_home/" && echo "restore $path_list done."
    fi
    docker restart jenkins
  fi
}


###########################
# main
###########################

case "$1" in
'sys')
  install_sys
  ;;
'build')
  build_images
  ;;
'all')
  install_sys
  build_images
  ;;
*)
  echo "Usage: $0 {sys|build|all <command>}" >&2
  exit 1
  ;;

esac

exit 0