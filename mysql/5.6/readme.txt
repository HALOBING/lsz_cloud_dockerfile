sources.list为apt-get的国内镜像，动态替换文件

windows vbox:

docker run \
--name mysql \
-p 3306:3306 \
--restart=always \
-e MYSQL_ROOT_PASSWORD=root \
-v /var/lib/mysql:/var/lib/mysql \
-v /var/log/mysql:/var/log/mysql \
-d mysql:5.6


在windows vbox下放到共享目录有问题无法启动mysql


linux:

docker run \
--name mysql \
-p 3306:3306 \
--restart=always \
-e MYSQL_ROOT_PASSWORD=root \
-v /docker-shared/mysql/data:/var/lib/mysql \
-v /docker-shared/mysql/log:/var/log/mysql \
-d mysql:5.6



需要先 cp mysqld.cnf /docker-shared/mysql/conf/mysqld.cnf && chomd 777 /docker-shared/mysql/conf/* 

docker run \
--name mysql \
-p 3306:3306 \
--restart=always \
-e MYSQL_ROOT_PASSWORD=root \
-v /docker-shared/mysql/data:/var/lib/mysql \
-v /docker-shared/mysql/log:/var/log/mysql \
-v /docker-shared/mysql/conf/mysqld.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf \
-d mysql:5.6 --lower_case_table_names=1