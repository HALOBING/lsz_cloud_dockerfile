#!/usr/bin/env bash

###########################
# docker
###########################

docker_install(){
    docker_pid=$(ps -ef | grep docker | grep -v 'grep' | awk '{print $2}')
#    if [ -z "$docker_pid" ] && [ ! -d "/var/lib/docker" ]; then
    if [ ! -d "/var/lib/docker" ]; then
        #install
        sudo dpkg --configure -a
        sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
        #sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        #sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        sudo curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
        sudo apt-get update
        sudo apt-get install -y docker-ce
        
        sudo chown -R ${USER} /etc/docker/*
        sudo chmod 775 /etc/docker/*
        sudo touch /etc/docker/daemon.json

        sudo cat > /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": [
    "https://dockerhub.azk8s.cn",
    "https://docker.mirrors.ustc.edu.cn",
    "https://registry.docker-cn.com"
  ],
  "graph": "/opt/docker",
  "log-driver":"json-file",
  "log-opts": {"max-size":"500m", "max-file":"3"}
}
EOF

        sudo systemctl stop docker
        # 默认目录设置
        sudo mv /var/lib/docker /opt/docker
        sudo systemctl daemon-reload
        sudo systemctl start docker
        sudo systemctl enable docker

        sudo docker info
    else
       if [ -z "$docker_pid" ]; then
         sudo systemctl start docker
       fi
       echo "The docker is already installed."
    fi
}

docker_uninstall(){
    sudo apt-get remove docker docker-engine docker-ce docker.io
    sudo rm -rf /opt/docker
    sudo rm -rf /var/lib/docker
}


###########################
# docker-compose
###########################

docker_compose_install(){
    docker_compose_pid=$(find / -name docker-compose)
    if [ -z "$docker_compose_pid" ]; then
        sudo pip3 install setuptools
        sudo pip3 install docker-compose
        docker-compose -version
    else
       echo "The docker-compose is already installed."
    fi
}

docker_compose_uninstall(){
   sudo pip3 uninstall docker-compose
}

###########################
# main
###########################

case "$1" in
  'install')
    docker_install
    docker_compose_install
  ;;
  'uninstall')
	docker_uninstall
	docker_compose_uninstall
  ;;
  *)
	echo "Usage: $0 {install|uninstall <command>}" >&2
	exit 1
  ;;

esac


exit 0
