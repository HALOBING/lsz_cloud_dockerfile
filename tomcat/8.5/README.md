######################################################
# 手动build docker tomcat镜像菜鸟笔记
# 试用于windows Docker Toolbox
######################################################

1、打开Oracle VM VirtualBox 
   创建位于d盘根目录的docker-shared作为docker与windows的共享目录
   重启docker虚拟机

   参考：http://note.youdao.com/noteshare?id=acc58215255547b435afd6f868cab460
   
2、拷贝该目录下的所有文件到d:/docker-shared/tomcat/下

3、进入docker命令行
分别键入命令
>cd /
>docker-machine ssh default
>cd /docker-shared/dockerfile/tomcat/
>docker build -t tomcat:8 .

（docker build -t tomcat8 . 最后的点代表当前目录下的Dockerfile文件）

4、创建tomcat8容器 指定数据卷 开机自启运行

docker run -d --name tomcat -p 8888:8080 --restart=always \
-v /docker-shared/tomcat/logs:/usr/local/apache-tomcat-8.5.34/logs \
tomcat:8.5


和mysql建立链接，使tomcat容器 能够通过 通过tcp aliasmysql:3306 访问mysql
docker run -d --name tomcat -p 8888:8080 --restart=always \
--link mysql:aliasmysql \
--link redis:aliasredis \
-v /docker-shared/tomcat/logs:/usr/local/apache-tomcat-8.5.34/logs \
tomcat:8.5


（主要：数据卷必须是和windows的共享目录，c盘根目录不行，c:/Users才行）

创建成功浏览器访问：http://192.168.99.100:8888/ (D:/docker-shared/tomcat/webapps/ROOT和D:/docker-shared/tomcat/logs要存在,会跳转到tomcat 404页面)

5、进入容器：

>docker exec -it 34a0fc061256 /bin/bash

6、在容器中安装vim

>apt-get update
>apt-get install vim

7、解决tomcat启动巨慢：（在build时已经替换修改好的java.security文件）
有2种解决方案：

1. 在Tomcat环境中解决：

可以通过配置 JRE 使用非阻塞的 Entropy Source：

在 catalina.sh 中加入这么一行：-Djava.security.egd=file:/dev/./urandom 即可。
 

2. 在 JVM 环境中解决（本人使用此方法）：

打开jdk安装路径 $JAVA_HOME/jre/lib/security/java.security 这个文件，找到下面的内容：

securerandom.source=file:/dev/random
替换成：
securerandom.source=file:/dev/./urandom




退出容器

退出容器，而不关闭容器：ctrl+P+Q；
退出容器，并且关闭容器：exit;

